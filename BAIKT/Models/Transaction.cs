﻿using System.ComponentModel.DataAnnotations;

namespace BAIKT.Models
{
    public class Transaction
    {
        public int Id { get; set; }
        public int EmployeesId { get; set; }
        public Customer? CustomerID { get; set; }
        public string Name { get; set; }
        public DateTime TransactionDate { get; set; }
        public decimal Amount { get; set; }
        public string? TransactionType { get; set; }
        public int CustomerId { get; set; }
        
   



        public Employee? Employee { get; set; }
        


    }
}
