﻿using System.ComponentModel.DataAnnotations;

namespace BAIKT.Models
{
    public class Log
    {
        public int Id { get; set; }
        public int? TransactionId { get; set; }
        public DateTime LogDateTime { get; set; }
        public DateTime LogginTime { get; set; }
        public Transaction Transaction { get; set; }



    }
}
