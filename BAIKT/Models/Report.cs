﻿using System.ComponentModel.DataAnnotations;

namespace BAIKT.Models
{
    public class Report
    {

        public int Id { get; set; }
        public int AccountId { get; set; }
        public int LogsId { get; set; }
        public int TransactionalId { get; set; }
        public string? ReportName { get; set; }
        public DateTime ReportDate { get; set; }

     
       
      


        public Transaction? Transaction { get; set; }
        public Log? Log { get; set; }
        public Account? Account { get; set; }



    }
}
